# your code goes here
def primeOrNot(test):
    targetList = []
    for i in range(test):
        num = int(input())
        count = 0
        for i in range(1, num + 1):
            if num % i == 0:
                count += 1
        if count == 2:
            targetList.append(str(num) + " is a prime")
        else :
            targetList.append(str(num) + " is not a prime")
    for j in targetList:
        print(j)
test = int(input())
primeOrNot(test)