from itertools import groupby
def get_anagrams(words):
  return [list(group) for key, group in groupby(sorted(words, key = sorted), sorted)]
print(get_anagrams(['country','rycount', 'ate', 'tea', 'eat']))
