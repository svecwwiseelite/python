def luckyDivision(num):
    if num % 4 == 0 or num % 7 == 0 or num % 47 == 0:
        return "YES"
    count = sum([1 for i in str(num) if int(i) in [4, 7]])
    if count == len(str(num)):
        return "YES"
    else :
        return "NO"
num = int(input())
print(luckyDivision(num))