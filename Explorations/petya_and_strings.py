def petyaString(word1, word2):
    count = 0
    for i, j in zip(list(word1.lower()), list(word2.lower())):
        if i == j:
            count += 1
        if i < j:
            return -1
        if i > j:
            return 1
    if count == len(word1):
        return 0
word1 = input()
word2 = input()
print(petyaString(word1, word2))