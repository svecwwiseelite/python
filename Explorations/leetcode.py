myDict = {1 : 'I', 4 : 'IV', 5 : 'V', 9 : 'IX', 10 : 'X', 40 : 'XL', 50 : 'L', 90 : 'XC',100 : 'C', 400 : 'CD', 500 : 'D', 900 : 'CM', 1000 : 'M'}
n = 20
li = [int(str(n)[i]) * 10 ** (len(str(n)) - i - 1) for i in range(len(str(n)))]
print(li)
targetStr = ""
for i in li:
    if i in myDict.keys():
      targetStr += myDict[i]
    elif i <= 3:
      targetStr += i * myDict[1]
    elif i <= 8:
      targetStr += myDict[5] + (i - 5) * myDict[1]
    elif i % 10 == 0 and i <= 50:
        targetStr += (i // 10) * myDict[10]
    elif i % 10 == 0 and i <= 100 :
      targetStr += myDict[50] + ((i - 50) // 10) * myDict[10]
    elif i % 100 == 0 and i <= 399:
      targetStr += (i // 100) * myDict[100]
    elif i % 100 == 0 and i <= 1000:
        targetStr += myDict[500] + ((i - 500) // 100) * myDict[100]
    elif i % 1000 == 0 :
        targetStr += (i // 1000) * myDict[1000]

print(targetStr)