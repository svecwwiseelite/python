def happyNumber(num):
    nums = []
    while num != 1:
        num = sum([int(i)** 2 for i in str(num)])
        if num in nums :
            return -1
        nums.append(num)
    if num == 1:
        return 1
print(happyNumber(int(input())))