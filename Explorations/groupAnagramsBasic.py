def group_anagrams(anagram_li):
  #li = ['cat', 'act', 'pen', 'nep', 'country', 'rycount']
  set_ana, dict_ana = set(), dict()
  for i in range(len(anagram_li)):
    set_ana.add(''.join(sorted(anagram_li[i])))
  for i in anagram_li:
    sort_str = ''.join(sorted(i))
    if sort_str in set_ana and sort_str in dict_ana.keys():
        dict_ana[sort_str] += "," + i
    else :
        dict_ana[sort_str] = i
  for i in dict_ana.values():
    str_ana = i.split(',')
    print(i)
group_anagrams(['tea', 'ate', 'eat', 'warn', 'arnw'])
