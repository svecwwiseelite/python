def checkSum(Nums, n):
    count = 0
    s = sum(Nums)
    if s == 0:
        while s == 0:
            if 0 in Nums:
                Nums[Nums.index(0)] += 1
                s = sum(Nums)
                count += 1
            else:
                for i in range(n):
                    Nums[i] += 1
                    count += 1
                    s = sum(Nums)
                    if s != 0:
                        break
    return count

T = int(input())
for i in range(T):
    n = int(input())
    Nums = [int(i) for i in input().split()]
    p = 1
    for i in Nums:
        p = p * i
    if sum(Nums) and p :
        print(0)
    else :
        count = 0
        s = sum(Nums)
        if s == 0:
            count += checkSum(Nums, n)
        if p == 0:
            while p == 0:
                for i in range(n):
                    if Nums[i] == 0:
                        Nums[i] += 1
                        count += 1
                    if p != 0:
                        break
                p = 1
                for i in Nums:
                    p = p * i
        count += checkSum(Nums, n)
        print(count)