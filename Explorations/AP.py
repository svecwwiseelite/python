testcases = int(input())
targetList = []
for i in range(testcases):
    li = [int(i) for i in input().split()]
    diff = ((li[1] - li[0]) // 5)
    ini = (li[0] - (3 * diff)) + 1
    sums = ini
    while sums <= li[-1]:
        targetList.append(ini)
        ini += 1
        sums += ini
    for i in targetList:
        print(i, end = " ")