import itertools
def dayOfProgrammer(year):   
    day = 256
    if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):
        months = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        monthsAccuSum = list(itertools.accumulate(months))
        print("leap",monthsAccuSum)   
    else :
        months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    monthsAccuSum = list(itertools.accumulate(months))
    print(monthsAccuSum)
    for i in monthsAccuSum:
        if str(i)[0] == str(day)[0] and day > i:
            mins = i
    month = monthsAccuSum.index(mins) + 2
    if str(month)[0] != "0":
        month = "0" + str(month)
    print(mins)
    days = day - mins
    targetStr = str(days) + "." + str(month) + "." + str(year)
    return targetStr 
print(dayOfProgrammer(1800))