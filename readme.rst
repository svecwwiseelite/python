**Introduction:**

Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. Python supports modules and packages, which encourages program modularity. Since there is no compilation step, the edit-test-debug cycle is incredibly fast. It contains many inbuilt functions that make code easier.

**Features:**

* Easy to code
* Object-Oriented Language
* GUI Programming Support
* Python is Portable language
* Large Standard Library

**Pre-requisites:**

* Ensure that sudo package is installed.

**Installation:**

* sudo apt-get install python

**To check the version:**

* python --version
 
**Resources:**

* `Python Documentation <docs.python.org>`_
 
* `Python for Beginners <https://wiki.python.org/moin/BeginnersGuide/Programmers>`_
 
* `Python Introduction <https://www.udacity.com/course/introduction-to-python--ud1110>`_
  
* `Python Education <https://developers.google.com/edu/python>`_
 
* `Getting Started with Python <https://www.coursera.org/learn/python>`_
 
* `Python Courses <https://www.udemy.com/topic/python/>`_
 
* `Learn Python <https://www.reddit.com/r/learnpython/wiki/index>`_
 
* `Python Tutorial <https://www.learnpython.org/>`_
 
* `Functional Programming Through Python <http://www.oreilly.com/programming/free/functional-programming-python.csp>`_
 
* `Python(including installation) <https://docs.python-guide.org/>`_

